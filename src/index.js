function limpaTabela (tabela) {
    tabela.innerHTML = '';
}

function generateTable (table, data) {
    for (let element of data) {
        let row = table.insertRow();
        for (key in element) {
            let cell = row.insertCell();
            let text = document.createTextNode(element[key]);
            cell.appendChild(text);
        }
    }
}

function gerarHorarios (dias, hora_inicio, hora_final, atraso_maximo) {
    let horarios = []

    for (let indice = 0; indice < dias; indice++) {
        let entrada = Math.floor((Math.random() * 15) + 0);
        let saida = 0

        if (Math.round((Math.random() * 1) + 0)) {
            saida = entrada + Math.floor((Math.random() * atraso_maximo) + 0);
        } else {
            saida = Math.abs(entrada - Math.floor((Math.random() * atraso_maximo) + 0));
        }

        entrada = hora_inicio.toString().padStart(2, '0') + ":" + entrada.toString().padStart(2, '0');
        saida = hora_final.toString().padStart(2, '0') + ":" + saida.toString().padStart(2, '0');

        horarios.push({ entrada, saida });
    }

    return horarios
}

function gerar(){
    // Inputs
    let table = document.querySelector("tbody");
    let atraso = document.querySelector("#atraso");
    let entrada = document.querySelector("#entrada");
    let saida = document.querySelector("#saida");
    let dias = document.querySelector("#dias");

    limpaTabela(table);
    generateTable(table, gerarHorarios(dias.value, entrada.value, saida.value, atraso.value));
}

// Código Principal

gerar();